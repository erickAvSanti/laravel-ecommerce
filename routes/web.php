<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	
    return view('welcome');

});

Route::group(['prefix' => 'website'], function() {
	Route::get('/classic', function () {
		
	    return view('website.classic');

	});
});

Route::group(['prefix' => 'control-panel'], function() {

    Auth::routes();

	Route::get('/home', 'HomeController@index')
		->name('home');

	Route::get('/products', 'ProductsController@index')
		->name('ecommerce_products')
		->middleware('auth');

	Route::get('/products/categories', 'ProductsCategoriesController@index')
		->name('ecommerce_products_categories')
		->middleware('auth');

	Route::get('/clients', 'ClientsController@index')
		->name('ecommerce_clients')
		->middleware('auth');

	Route::get('/cover-images', 'CoverImagesController@index')
		->name('ecommerce_cover_images')
		->middleware('auth');

	Route::get('/options', 'OptionsController@index')
		->name('ecommerce_options')
		->middleware('auth');

	Route::get('/crontab', 'CronTabController@index')
		->name('ecommerce_crontab')
		->middleware('auth');
});

Route::group(['prefix' => 'ws'], function() {
	Route::group(['prefix' => 'control-panel'], function() {
		Route::group(['prefix' => 'products'], function() {

			Route::post('/', 'ProductsController@create')
				->name('ecommerce_products_post');

			Route::put('/{id}', 'ProductsController@update')
				->name('ecommerce_products_put')->where('id','\d+');

			Route::delete('/{id}', 'ProductsController@remove')
				->name('ecommerce_products_delete')->where('id','\d+');


			Route::post('/categories', 'ProductsCategoriesController@create')
				->name('ecommerce_products_categories_post');

			Route::put('/categories/{id}', 'ProductsCategoriesController@update')
				->name('ecommerce_products_categories_put')->where('id','\d+');

			Route::delete('/categories/{id}', 'ProductsCategoriesController@remove')
				->name('ecommerce_products_categories_delete')->where('id','\d+');

		});
		
		Route::group(['prefix' => 'clients'], function() {

			Route::post('/', 'ClientsController@create')
				->name('ecommerce_clients_post');

			Route::put('/{id}', 'ClientsController@update')
				->name('ecommerce_clients_put')->where('id','\d+');

			Route::delete('/{id}', 'ClientsController@remove')
				->name('ecommerce_clients_delete')->where('id','\d+');

		});
		
		Route::group(['prefix' => 'cover-images'], function() {

			Route::get('/', 'CoverImagesController@list')
				->name('ecommerce_cover_images_get');

			Route::post('/', 'CoverImagesController@create')
				->name('ecommerce_cover_images_post');

			Route::put('/{id}', 'CoverImagesController@update')
				->name('ecommerce_cover_images_put')->where('id','\d+');

			Route::delete('/{id}', 'CoverImagesController@remove')
				->name('ecommerce_cover_images_delete')->where('id','\d+');

		});
		
		Route::group(['prefix' => 'options'], function() {

			Route::get('/', 'OptionsController@list')
				->name('ecommerce_options_get');

			Route::post('/', 'OptionsController@create')
				->name('ecommerce_options_post');

			Route::put('/{id}', 'OptionsController@update')
				->name('ecommerce_options_put')->where('id','\d+');

			Route::delete('/{id}', 'OptionsController@remove')
				->name('ecommerce_options_delete')->where('id','\d+');

		});
		
		Route::group(['prefix' => 'crontab'], function() {

			Route::get('/', 'CronTabController@list')
				->name('ecommerce_crontab_get');

			Route::post('/', 'CronTabController@create')
				->name('ecommerce_crontab_post');

			Route::put('/{id}', 'CronTabController@update')
				->name('ecommerce_crontab_put')->where('id','\d+');

			Route::delete('/{id}', 'CronTabController@remove')
				->name('ecommerce_crontab_delete')->where('id','\d+');

			Route::post('/save', 'CronTabController@save_to_file')
				->name('ecommerce_crontab_save_to_file');

		});

	});
	
});

