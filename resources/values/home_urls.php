<?php 
	return [
		['name' => 'Productos', 'route_name' => 'ecommerce_products'],
		['name' => 'Categorias de Productos', 'route_name' => 'ecommerce_products_categories'],
		['name' => 'Clientes', 'route_name' => 'ecommerce_clients'],
		['name' => 'Imágenes de portada', 'route_name' => 'ecommerce_cover_images'],
		['name' => 'Opciones de la web', 'route_name' => 'ecommerce_options'],
		['name' => 'Cron Jobs', 'route_name' => 'ecommerce_crontab'],
	];