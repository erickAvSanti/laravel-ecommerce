<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'product' => 'Product',
    'products' => 'Products',
    'products_categories' => 'Products Categories',

    'product' => 'Client',
    'products' => 'Clients',
    'cover_images' => 'Cover images',
    'options' => 'Options',
    'crontab' => 'Cron Jobs',

];
