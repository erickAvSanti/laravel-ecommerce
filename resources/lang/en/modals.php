<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'edit_product' => 'Edit product',
    'add_product' => 'Add product',
    'product_name' => 'Product name',

    'edit_product_subcategory' => 'Edit sub-category',
    'add_product_subcategory' => 'Add sub-category',
    'product_subcategory_name' => 'Sub-Category name',

    'edit_product_category' => 'Edit category',
    'add_product_category' => 'Add category',
    'product_category_name' => 'Category name',

    'close_default' => 'Close',
    'dismiss_default' => 'Dismiss',
    'ok_default' => 'Ok',
    'save_default' => 'Save',

    'edit_client' => 'Edit client',
    'add_client' => 'Add client',
    'client_name' => 'Client name',

    'option_key' => 'Key',
    'option_value' => 'Value',
    'option_desc' => 'Description',

    'edit_option' => 'Edit option',
    'add_option' => 'Add option',
    'option_name' => 'Option name',

    'crontab_minute' => 'Minute',
    'crontab_hour' => 'Hour',
    'crontab_day_of_month' => 'Day of month',
    'crontab_month' => 'Month',
    'crontab_day_of_week' => 'Day of week',
    'crontab_title' => 'CronJob task',
    'crontab_command' => 'Command',

    'add_crontab' => 'Add cron job',
    'edit_crontab' => 'Edit cron job',

];
