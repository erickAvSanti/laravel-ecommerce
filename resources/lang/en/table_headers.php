<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'created_at' => 'Created at',
    'updated_at' => 'Updated at',
    'name' => 'Name',
    
    'crontab_minute' => 'Minute',
    'crontab_hour' => 'Hour',
    'crontab_day_of_month' => 'Day of month',
    'crontab_month' => 'Month',
    'crontab_day_of_week' => 'Day of week',
    'crontab_command' => 'Command',

];
