<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'edit_product' => 'Editar producto',
    'add_product' => 'Agregar producto',
    'product_name' => 'Nombre del producto',

    'edit_product_subcategory' => 'Editar sub-categoría',
    'add_product_subcategory' => 'Agregar sub-categoría',
    'product_subcategory_name' => 'Nombre de la sub-categoría',

    'edit_product_category' => 'Editar categoría',
    'add_product_category' => 'Agregar categoría',
    'product_category_name' => 'Nombre de la categoría',
    
    'close_default' => 'Cerrar',
    'dismiss_default' => 'Descartar',
    'ok_default' => 'Ok',
    'save_default' => 'Guardar',

    'edit_client' => 'Editar cliente',
    'add_client' => 'Agregar cliente',
    'client_name' => 'Nombre del cliente',

    'option_key' => 'Clave',
    'option_value' => 'Valor',
    'option_desc' => 'Descripción',

    'edit_option' => 'Editar opción',
    'add_option' => 'Agregar opción',
    'option_name' => 'Nombre de la opción',

    'crontab_minute' => 'Minuto',
    'crontab_hour' => 'Hora',
    'crontab_day_of_month' => 'Día de mes',
    'crontab_month' => 'Mes',
    'crontab_day_of_week' => 'Día de semana',
    'crontab_title' => 'Tarea CronJob',
    'crontab_command' => 'Comando',

    'add_crontab' => 'Añadir cron job',
    'edit_crontab' => 'Editar cron job',

];
