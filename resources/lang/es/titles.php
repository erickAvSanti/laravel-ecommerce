<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'product' => 'Producto',
    'products' => 'Productos',
    'products_categories' => 'Categorías de Productos',

    'client' => 'Cliente',
    'clients' => 'Clientes',
    'cover_images' => 'Imágenes de portada',
    'options' => 'Opciones',
    'crontab' => 'Trabajos Cron',
];
