<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
    'name' => 'Nombre',

    'crontab_minute' => 'Minuto',
    'crontab_hour' => 'Hora',
    'crontab_day_of_month' => 'Día de mes',
    'crontab_month' => 'Mes',
    'crontab_day_of_week' => 'Día de semana',
    'crontab_command' => 'Comando',

];
