const url_ws = '/ws/control-panel/crontab'

jQuery(window).ready(function(){

	jQuery('.crontabs-remove').click(function( evt ){
		
		let _this = jQuery( this )
		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )

		Swal.fire({

			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'DELETE',
					url: `${url_ws}/${ json.id}`,
					data: {},
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Deleted!','Your record has been deleted.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})
	jQuery('.crontabs-edit').click(function( evt ){

		hide_msg_empties()

		let _this = jQuery( this )
		jQuery( '#crontabs-form-label' ).html( jQuery( '#crontabs-form-label' ).attr( 'edit-name' ) )

		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )
		console.log( 'json' , json )
		let tags = jQuery( '#crontabs-form .modal-body input[name]' )
		tags.each(( index , tag ) =>{
			let dom = jQuery( tag )
			let iname = dom.attr( 'name' )
			console.log( 'iname' , iname , tag )
			if(iname in json) dom.val( json[iname] )
		}) 
		tags = jQuery( '#crontabs-form .modal-body textarea[name]' )
		tags.each(( index , tag ) =>{
			let dom = jQuery( tag )
			let iname = dom.attr( 'name' )
			console.log( 'iname' , iname , tag )
			if(iname in json) dom.val( json[iname] )
		}) 

		jQuery( '#crontabs-form').modal( 'show' )
	})
	jQuery('#crontabs-add').click(function( evt ){
		jQuery( '#crontabs-form .modal-body input[name=id]' ).val( '' )
		jQuery( '#crontabs-form-label' ).html( jQuery( '#crontabs-form-label' ).attr( 'add-name' ) )
		hide_msg_empties()
	})

	jQuery( '#crontabs-form .btn.btn-primary' ).click(function( evt ){

		let data = {}
		let tags = jQuery( '#crontabs-form .modal-body input[name]' )
		for( let i of tags )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		tags = jQuery( '#crontabs-form .modal-body textarea[name]' )
		for( let i of tags )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		console.log( "make post " , data )
		axios({
			method: ( data.id  ? 'PUT' : 'POST' ),
			url: `${ url_ws }${ data.id ? `/${ data.id }` : '' }`,
			data: data,
			headers:{'Content-Type':'application/json','Accept':'application/json'},
		}).then(( response ) => {

			console.log( response )
			if( response.status == 200 )
			{
				Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'})
				window.location.reload()
			}

		}).catch(( error ) => {

			console.log( error.response )

			if( error.response.status == 403 )
			{
				jQuery('#session-expired').modal('show')

			}else if( error.response.status == 400  )
			{
				Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
			}else
			{
				Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
			}

		})
	})

	jQuery( '#card-crontabs .reload-table' ).click(function( evt ){
		window.location.reload()
	})

	jQuery( '#save_to_file' ).click(function( evt ){



		Swal.fire({

			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, save it!'

		}).then((result) => {
			if (result.value) {
		
				axios({
					method: 'POST',
					url: `${ url_ws }/save`,
					data: {},
					headers:{'Content-Type':'application/json','Accept':'application/json'},
				}).then(( response ) => {

					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'}) 
					}

				}).catch(( error ) => {

					console.log( error.response )

					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}

				})

			}
		})
	})

})

function hide_msg_empties()
{
	jQuery( '#crontabs-form .modal-body .msg_empty' ).removeClass('show').addClass('hide')
}