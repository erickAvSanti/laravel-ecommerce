const url_options = '/ws/control-panel/options'

jQuery(window).ready(function(){

	jQuery('.options-remove').click(function( evt ){
		
		let _this = jQuery( this )
		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )

		Swal.fire({

			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'DELETE',
					url: `${url_options}/${ json.id}`,
					data: {},
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Deleted!','Your record has been deleted.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})
	jQuery('.options-edit').click(function( evt ){

		hide_msg_empties()

		let _this = jQuery( this )
		jQuery( '#options-form-label' ).html( jQuery( '#options-form-label' ).attr( 'edit-name' ) )

		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )
		console.log( 'json' , json )
		let inputs = jQuery( '#options-form .modal-body [name]' )
		inputs.each(( index , input ) =>{
			let dom = jQuery( input )
			let iname = dom.attr( 'name' )
			console.log( 'iname' , iname , input )
			if(iname in json) dom.val( json[iname] )
		}) 

		jQuery( '#options-form').modal( 'show' )
	})
	jQuery('#options-add').click(function( evt ){
		jQuery( '#options-form .modal-body input[name=id]' ).val( '' )
		jQuery( '#options-form-label' ).html( jQuery( '#options-form-label' ).attr( 'add-name' ) )
		hide_msg_empties()
	})

	jQuery( '#options-form .btn.btn-primary' ).click(function( evt ){

		let inputs = jQuery( '#options-form .modal-body [name]' )
		let data = {}
		for( let i of inputs )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		console.log( "make post " , data )
		axios({
			method: ( data.id  ? 'PUT' : 'POST' ),
			url: `${ url_options }${ data.id ? `/${ data.id }` : '' }`,
			data: data,
			headers:{'Content-Type':'application/json','Accept':'application/json'},
		}).then(( response ) => {

			console.log( response )
			if( response.status == 200 )
			{
				Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'})
				window.location.reload()
			}

		}).catch(( error ) => {

			console.log( error.response )

			if( error.response.status == 403 )
			{
				jQuery('#session-expired').modal('show')

			}else if( error.response.status == 400  )
			{
				Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
			}else
			{
				Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
			}

		})
	})
	jQuery( '#card-options .reload-table' ).click(function( evt ){
		window.location.reload()
	})

})

function hide_msg_empties()
{
	jQuery( '#options-form .modal-body .msg_empty' ).removeClass('show').addClass('hide')
}