const url_clients = '/ws/control-panel/clients'

jQuery(window).ready(function(){

	jQuery('.clients-remove').click(function( evt ){
		
		let _this = jQuery( this )
		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )

		Swal.fire({

			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'DELETE',
					url: `${url_clients}/${ json.id}`,
					data: {},
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Deleted!','Your record has been deleted.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})
	jQuery('.clients-edit').click(function( evt ){

		hide_msg_empties()

		let _this = jQuery( this )
		jQuery( '#clients-form-label' ).html( jQuery( '#clients-form-label' ).attr( 'edit-name' ) )

		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )
		console.log( 'json' , json )
		let inputs = jQuery( '#clients-form .modal-body input[name]' )
		inputs.each(( index , input ) =>{
			let dom = jQuery( input )
			let iname = dom.attr( 'name' )
			console.log( 'iname' , iname , input )
			if(iname in json) dom.val( json[iname] )
		}) 

		jQuery( '#clients-form').modal( 'show' )
	})
	jQuery('#clients-add').click(function( evt ){
		jQuery( '#clients-form .modal-body input[name=id]' ).val( '' )
		jQuery( '#clients-form-label' ).html( jQuery( '#clients-form-label' ).attr( 'add-name' ) )
		jQuery( '#clients-form .modal-body .categories label[item-id]' ).removeClass( 'checked' )
		hide_msg_empties()
	})

	jQuery('#clients_form .page-item:not(.disabled):not(.active)').click(function(evt){
		evt.stopPropagation()
		evt.preventDefault()
		let _this = $( this )
		let page = _this.attr( 'page' )
		if(page !='...' )
		{
			jQuery( '#clients_form input[name=page]' ).val( page )
			jQuery( '#clients_form' ).submit()
		}
	})

	jQuery( '#clients-form .btn.btn-primary' ).click(function( evt ){

		let inputs = jQuery( '#clients-form .modal-body input[name]' )
		let data = {}
		for( let i of inputs )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		console.log( "make post " , data )
		axios({
			method: ( data.id  ? 'PUT' : 'POST' ),
			url: `${ url_clients }${ data.id ? `/${ data.id }` : '' }`,
			data: data,
			headers:{'Content-Type':'application/json','Accept':'application/json'},
		}).then(( response ) => {

			console.log( response )
			if( response.status == 200 )
			{
				Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'})
				window.location.reload()
			}

		}).catch(( error ) => {

			console.log( error.response )

			if( error.response.status == 403 )
			{
				jQuery('#session-expired').modal('show')

			}else if( error.response.status == 400  )
			{
				Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
			}else
			{
				Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
			}

		})
	})
	jQuery( '#reload-table' ).click(function( evt ){
		jQuery( '#clients_form' ).submit()
	})

})

function hide_msg_empties()
{
	jQuery( '#clients-form .modal-body .msg_empty' ).removeClass('show').addClass('hide')
}