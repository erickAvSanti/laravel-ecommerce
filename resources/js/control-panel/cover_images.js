
const url_ws = '/ws/control-panel/cover-images'
jQuery( window ).ready(function(){

	let dom = document.getElementById( 'dpzci' )
	if(!dom)return
	let dz = Dropzone.forElement( "#dpzci" )
	console.log( dz )
	dz.options.uploadMultiple = true
	dz.options.maxFilesize = 5
	dz.options.acceptedFiles = 'image/*'
	/*
	//if uploadMultiple is false
	dz.on("success",function(file){
		console.log(file)
		getCoverImages()
	})
	*/
	dz.on("successmultiple",function(files){
		window.location.reload()
	})

	jQuery( '.remove-image' ).click( function(evt){
		let dom = jQuery( this )
		let id = dom.attr('item-id')

		Swal.fire({

			title: 'Are you sure to delete?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'DELETE',
					url: `${ url_ws }/${ id }`,
					data: {},
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Deleted!','Your record has been deleted.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})

	jQuery( '.edit-image' ).click( function(evt){
		let dom = jQuery( this )
		let id = dom.attr('item-id')
		let inputs = dom.parent().find('[name]')
		let data = {}
		inputs.each(( index , input ) =>{
			let dom = jQuery( input )
			let name = dom.attr( 'name' )
			data[name] = dom.val()
		}) 

		Swal.fire({

			title: 'Edit information',
			text: "You will be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, edit it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'PUT',
					url: `${ url_ws }/${ id }`,
					data: data,
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Edited!','Your record has been edited.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})

})

function getCoverImages()
{
	axios({
		method: 'GET',
		url: url_ws,
		data: {},
		headers:{
			'Content-Type':'application/json',
			'Accept':'application/json',
		},
	}).then(( response ) => {
		console.log( response )

	}).catch(( error ) => {
		console.log( error.response )
		if( error.response.status == 403 )
		{
			jQuery('#session-expired').modal('show')

		}
	})
}