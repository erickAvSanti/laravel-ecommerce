const url_ws = '/ws/control-panel/products/categories'

jQuery(window).ready(function(){

	jQuery('.products-categories-remove').click(function( evt ){
		
		let _this = jQuery( this )
		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )

		Swal.fire({

			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'DELETE',
					url: `${url_ws}/${ json.id}`,
					data: {},
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Deleted!','Your record has been deleted.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})
	jQuery('.products-categories-edit').click(function( evt ){

		hide_msg_empties()

		let _this = jQuery( this )
		jQuery( '#products-categories-form-label' ).html( jQuery( '#products-categories-form-label' ).attr( 'edit-name' ) )

		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )
		console.log( 'json' , json )
		let inputs = jQuery( '#products-categories-form .modal-body input[name]' )
		inputs.each(( index , input ) =>{
			let dom = jQuery( input )
			let iname = dom.attr( 'name' )
			console.log( 'iname' , iname , input )
			if(iname in json) dom.val( json[iname] )
		}) 

		jQuery( '#products-categories-form').modal( 'show' )
	})
	jQuery('#products-categories-add').click(function( evt ){
		jQuery( '#products-categories-form .modal-body input[name=id]' ).val( '' )
		jQuery( '#products-categories-form-label' ).html( jQuery( '#products-categories-form-label' ).attr( 'add-name' ) )
		hide_msg_empties()
	})

	jQuery('.products-subcategories-add').click(function( evt ){
		jQuery( '#products-subcategories-form .modal-body input[name=id]' ).val( '' )
		jQuery( '#products-subcategories-form .modal-body input[name=parent_id]' ).val( jQuery(this).parent().attr( 'row-data-id' ) )
		jQuery( '#products-subcategories-form-label' ).html( jQuery( '#products-subcategories-form-label' ).attr( 'add-name' ) )
		jQuery( '#products-subcategories-form' ).modal( 'show' )
		hide_msg_empties2()
	})

	jQuery( '#products-categories-form .btn.btn-primary' ).click(function( evt ){

		let inputs = jQuery( '#products-categories-form .modal-body input[name]' )
		let data = {}
		for( let i of inputs )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		console.log( "make post " , data )
		axios({
			method: ( data.id  ? 'PUT' : 'POST' ),
			url: `${ url_ws }${ data.id ? `/${ data.id }` : '' }`,
			data: data,
			headers:{'Content-Type':'application/json','Accept':'application/json'},
		}).then(( response ) => {

			console.log( response )
			if( response.status == 200 )
			{
				Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'})
				window.location.reload()
			}

		}).catch(( error ) => {

			console.log( error.response )

			if( error.response.status == 403 )
			{
				jQuery('#session-expired').modal('show')

			}else if( error.response.status == 400  )
			{
				Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
			}else
			{
				Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
			}

		})
	})

	/* sub categories events of level 1*/
	jQuery( '#products-subcategories-form .btn.btn-primary' ).click(function( evt ){

		let inputs = jQuery( '#products-subcategories-form .modal-body input[name]' )
		let data = {}
		for( let i of inputs )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		console.log( "make post " , data )
		axios({
			method: 'POST',
			url: url_ws,
			data: data,
			headers:{'Content-Type':'application/json','Accept':'application/json'},
		}).then(( response ) => {

			console.log( response )
			if( response.status == 200 )
			{
				Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'})
				window.location.reload()
			}

		}).catch(( error ) => {

			console.log( error.response )

			if( error.response.status == 403 )
			{
				jQuery('#session-expired').modal('show')

			}else if( error.response.status == 400  )
			{
				Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
			}else
			{
				Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
			}

		})
	})
	jQuery( '#card-product-categories .reload-table' ).click(function( evt ){
		window.location.reload()
	})

})

function hide_msg_empties()
{
	jQuery( '#products-categories-form .modal-body .msg_empty' ).removeClass('show').addClass('hide')
}
function hide_msg_empties2()
{
	jQuery( '#products-subcategories-form .modal-body .msg_empty' ).removeClass('show').addClass('hide')
}