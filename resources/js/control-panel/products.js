const url_products = '/ws/control-panel/products'

jQuery(window).ready(function(){

	jQuery('.products-remove').click(function( evt ){
		
		let _this = jQuery( this )
		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )

		Swal.fire({

			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'

		}).then((result) => {
			if (result.value) {

				axios({
					method: 'DELETE',
					url: `${url_products}/${ json.id}`,
					data: {},
					headers:{
						'Content-Type':'application/json',
						'Accept':'application/json',
					},
				}).then(( response ) => {
					console.log( response )
					if( response.status == 200 )
					{
						Swal.fire('Deleted!','Your record has been deleted.','success')
						window.location.reload()
					}

				}).catch(( error ) => {
					console.log( error.response )
					if( error.response.status == 403 )
					{
						jQuery('#session-expired').modal('show')

					}else if( error.response.status == 400  )
					{
						Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
					}else
					{
						Swal.fire({title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
					}
				})
			}
		})

	})
	jQuery('.products-edit').click(function( evt ){

		hide_msg_empties()

		let _this = jQuery( this )
		jQuery( '#products-form-label' ).html( jQuery( '#products-form-label' ).attr( 'edit-name' ) )

		let json = jQuery.parseJSON( _this.parent().attr( 'data-json' ) )
		console.log( 'json' , json )
		let inputs = jQuery( '#products-form .modal-body input[name]' )
		inputs.each(( index , input ) =>{
			let dom = jQuery( input )
			let iname = dom.attr( 'name' )
			console.log( 'iname' , iname , input )
			if(iname in json) dom.val( json[iname] )
		}) 
		let category_ids = json.category_ids
		jQuery( `#products-form .modal-body .categories label[item-id]` ).removeClass( 'checked' )
		if(category_ids && category_ids.length)
		{
			for(let cat_id of category_ids)
			{
				jQuery( `#products-form .modal-body .categories label[item-id=${cat_id}]` ).addClass( 'checked' )
			}
		}

		jQuery( '#products-form').modal( 'show' )
	})
	jQuery('#products-add').click(function( evt ){
		jQuery( '#products-form .modal-body input[name=id]' ).val( '' )
		jQuery( '#products-form-label' ).html( jQuery( '#products-form-label' ).attr( 'add-name' ) )
		jQuery( '#products-form .modal-body .categories label[item-id]' ).removeClass( 'checked' )
		hide_msg_empties()
	})

	jQuery('#products_form .page-item:not(.disabled):not(.active)').click(function(evt){
		evt.stopPropagation()
		evt.preventDefault()
		let _this = $( this )
		let page = _this.attr( 'page' )
		if(page !='...' )
		{
			jQuery( '#products_form input[name=page]' ).val( page )
			jQuery( '#products_form' ).submit()
		}
	})

	jQuery( '#products-form .btn.btn-primary' ).click(function( evt ){

		let inputs = jQuery( '#products-form .modal-body input[name]' )
		let data = {}
		for( let i of inputs )
		{
			let ii = jQuery( i )
			let name = ii.attr( 'name' )
			if( name != 'id' )ii.parent().find( '.msg_empty' ).removeClass('show').addClass('hide')
			let _val = ii.val()
			if( !!!_val && name != 'id' ){
				ii.parent().find( '.msg_empty' ).removeClass('hide').addClass('show')
				return
			}
			data[ jQuery(i).attr('name') ] = _val

		}
		let categories = []
		jQuery( '.categories label[item-id].checked' ).each( function(idx,label){
			categories.push( parseInt( jQuery( this ).attr( 'item-id' ) ) );
		})
		data.categories = JSON.stringify(categories)
		console.log( "make post " , data )
		axios({
			method: ( data.id  ? 'PUT' : 'POST' ),
			url: `${ url_products }${ data.id ? `/${ data.id }` : '' }`,
			data: data,
			headers:{'Content-Type':'application/json','Accept':'application/json'},
		}).then(( response ) => {

			console.log( response )
			if( response.status == 200 )
			{
				Swal.fire({title: 'Success!',text: '',icon: 'success',confirmButtonText: 'Ok'})
				window.location.reload()
			}

		}).catch(( error ) => {

			console.log( error.response )

			if( error.response.status == 403 )
			{
				jQuery('#session-expired').modal('show')

			}else if( error.response.status == 400  )
			{
				Swal.fire({title: 'Bad request!',icon: 'warning',confirmButtonText: 'Ok'})
			}else
			{
				Swal.fire({ title: 'Error!',icon: 'error',confirmButtonText: 'Ok'})
			}

		})
	})
	jQuery( '#card-products .reload-table' ).click(function( evt ){
		jQuery( '#products_form' ).submit()
	})

	jQuery( '#products-form .modal-body .categories label' ).click( function(evt){
		let dom = jQuery(this)
		if( dom.parent().hasClass('children') )
		{
			let dom_has_checked = dom.hasClass( 'checked' )
			dom.parent().find( 'label' ).each( (idx,label) =>{
				if( !dom_has_checked ){
					jQuery(label).addClass( 'checked' )
				}else{
					jQuery(label).removeClass( 'checked' )
				}
			})
		}else{
			dom.toggleClass( 'checked' )
			if( dom.parent().parent().children( 'li' ).length == 1 )
			{
				if( dom.hasClass( 'checked' ) )
				{
					dom.parent().parent().parent().children( 'label' ).addClass(  'checked' )
				}else{
					dom.parent().parent().parent().children( 'label' ).removeClass(  'checked' )
				}
			}
			if( dom.parent().parent().parent().parent().children( 'li' ).length == 1 )
			{
				if( dom.hasClass( 'checked' ) )
				{
					dom.parent().parent().parent().parent().parent().children( 'label' ).addClass(  'checked' )
				}else{
					dom.parent().parent().parent().parent().parent().children( 'label' ).removeClass(  'checked' )
				}
			}
		}
	})

})

function hide_msg_empties()
{
	jQuery( '#products-form .modal-body .msg_empty' ).removeClass('show').addClass('hide')
}