<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <title>Classic</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/website/classic.css') }}" rel="stylesheet">

    </head>
    <body>
        <header class="aqg-header-wrapper">
            <div class="aqg-header">
                <div class="aqg-row">
                    <div class="aqg-logo">
                        <a href="">
                            <img src="{{ asset('img1.jpg') }}">
                        </a>
                    </div>
                    <nav class="aqg-menu">
                        <ul>
                            <li>
                                <a href="#">
                                    <span>Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>About</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Contact us</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>News</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    </body>
</html>
