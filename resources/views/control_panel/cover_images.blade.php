@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header custom-design1">
                    {{__('titles.cover_images')}}
                </div>

                <div class="card-body custom-design1">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="/ws/control-panel/cover-images"
                        class="dropzone"
                        id="dpzci">
                            @csrf
                    </form>
                    <div class="control-panel cover-images">
                    @foreach($records as &$record)
                        <div class="row">
                            <div class="col-md-6 img custom-scrollbar1">
                                <img src="{{ asset('').$relative_path.$record->filename }}">
                            </div>
                            <div class="col-md-6">
                                <div class="btn btn-danger remove-image btn-sm" item-id='{{ $record->id }}' data-toggle="tooltip" data-placement="top" title="Delete all record information">
                                    <i class="fa fa-times"></i>
                                </div>
                                <div class="btn btn-success edit-image btn-sm" item-id='{{ $record->id }}' data-toggle="tooltip" data-placement="top" title="Edit only text">
                                    <i class="fa fa-pencil"></i>
                                </div><br /><br />
                                <div class="form-group">
                                    <label for="title1">Title</label>
                                    <textarea class="form-control" name="title" rows="2">{{ $record->title ? $record->title : '' }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="subtitle1">Subtitle1</label>
                                    <textarea class="form-control" name="subtitle1" rows="2">{{ $record->subtitle1 ? $record->subtitle1 : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    @endforeach 
                    </div>          
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection