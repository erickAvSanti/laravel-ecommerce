@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header custom-design1">
                    {{__('titles.crontab')}}
                </div>

                <div id="card-crontabs" class="card-body custom-design1">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="full-scroll-x">
                        <table class="table table-hover table-borderer">
                            <thead>
                                <th>
                                    <div id="crontabs-add" data-toggle="modal" data-target="#crontabs-form" class="btn-sm btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                </th>
                                <th>{{ __('table_headers.crontab_minute') }}</th>
                                <th>{{ __('table_headers.crontab_hour') }}</th>
                                <th>{{ __('table_headers.crontab_day_of_month') }}</th>
                                <th>{{ __('table_headers.crontab_month') }}</th>
                                <th>{{ __('table_headers.crontab_day_of_week') }}</th>
                                <th>{{ __('table_headers.crontab_command') }}</th>
                                <th>{{ __('table_headers.updated_at') }}</th>
                                <!-- 
                                <th>{{ __('table_headers.created_at') }}</th>
                                -->
                                <th>
                                    <div class="btn-sm btn btn-success reload-table">
                                        <i class="fa fa-sync"></i>
                                    </div>
                                    <div class="btn-sm btn btn-outline-success" id="save_to_file">
                                        <i class="fa fa-save"></i>
                                    </div>
                                </th>
                            </thead>
                            <tbody>
                                @php
                                    $count_table_row = 1;
                                @endphp

                                @foreach($records as $record)
                                <tr>
                                    <td>{{ $count_table_row++ }}</td>
                                    <td>{{ $record->c_minute }}</td>
                                    <td>{{ $record->c_hour }}</td>
                                    <td>{{ $record->c_day_of_month }}</td>
                                    <td>{{ $record->c_month }}</td>
                                    <td>{{ $record->c_day_of_week }}</td>
                                    <td>{{ $record->c_command }}</td>
                                    <td>{{ \Carbon\Carbon::parse( $record->updated_at )->format('d/m/Y H:i:s') }}</td>
                                    <!--
                                    <td>{{ \Carbon\Carbon::parse( $record->created_at )->format('d/m/Y H:i:s') }}</td>
                                    -->
                                    <td row-data-id='{{ $record->id }}' style='min-width: 110px;' data-json = '{{ json_encode($record) }}'>
                                        <div class="crontabs-edit btn-sm btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </div>
                                        <div class="crontabs-remove btn-sm btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="crontabs-form" tabindex="-1" role="dialog" aria-labelledby="crontabs-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="crontabs-form-label" add-name="{{__('modals.add_crontab')}}" edit-name="{{__('modals.edit_crontab')}}">{{__('modals.add_crontab')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <div class="form-group">
                    <label for="crontab-name">{{ __('modals.crontab_minute')}}</label>
                    <input type="text" name="c_minute" class="form-control" id="crontab-minute" placeholder="{{__('modals.crontab_minute')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label for="crontab-hour">{{ __('modals.crontab_hour')}}</label>
                    <input type="text" name="c_hour" class="form-control" id="crontab-hour" placeholder="{{__('modals.crontab_hour')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label for="crontab-day-of-month">{{ __('modals.crontab_day_of_month')}}</label>
                    <input type="text" name="c_day_of_month" class="form-control" id="crontab-day-of-month" placeholder="{{__('modals.crontab_day_of_month')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label for="crontab-month">{{ __('modals.crontab_month')}}</label>
                    <input type="text" name="c_month" class="form-control" id="crontab-month" placeholder="{{__('modals.crontab_month')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label for="crontab-day-of-week">{{ __('modals.crontab_day_of_week')}}</label>
                    <input type="text" name="c_day_of_week" class="form-control" id="crontab-day-of-week" placeholder="{{__('modals.crontab_day_of_week')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label for="crontab-command">{{ __('modals.crontab_command')}}</label>
                    <textarea class="form-control" name="c_command" id="crontab-command" rows="3"></textarea>
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modals.close_default') }}</button>
                <button type="button" class="btn btn-primary">{{__('modals.save_default')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection