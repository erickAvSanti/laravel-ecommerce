@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header custom-design1">
                    {{__('titles.clients')}}
                </div>

                <div class="card-body custom-design1">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="clients_form" action="{{route('ecommerce_clients')}}" method="GET">
                        
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item {{ $data[ 'page' ] == 1 ? 'disabled':'' }}" page ="{{ $data['page']>1 ? ($data['page'] - 1) : 1 }}">
                                    <span class="page-link">{{ __('pagination.previous') }}</span>
                                </li>
                                @foreach($pagination as $i)
                                    @if($i == $data['page'])
                                        <li class="page-item active" page="{{ $i }}" aria-current="page">
                                            <span class="page-link">
                                                {{ $i }}
                                                <span class="sr-only">(current)</span>
                                            </span>
                                        </li>
                                    @else
                                        <li class="page-item" page="{{ $i }}">
                                            <span class="page-link">{{ $i }}</span>
                                        </li>
                                    @endif
                                @endforeach
                                <li class="page-item {{ $data[ 'page' ] == $data['total_pages'] ? 'disabled':'' }}" page ="{{ $data['page'] < $data['total_pages'] ? ($data['page'] + 1) : $data['total_pages'] }}">
                                
                                    <span class="page-link">{{ __('pagination.next') }}</span>

                                </li>
                            </ul>
                            <input type="hidden" name="page" value="1">
                        </nav>
                         <div class="form-group row">
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="clients-search-label">{{__('misc.search')}}</span>
                                    </div>
                                    <input type="text" name="search" value="{{ $search }}" class="form-control" id="clients-search" aria-describedby="clients-search-label">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="full-scroll-x">
                        <table class="table table-hover table-borderer">
                            <thead>
                                <th>
                                    <div id="clients-add" data-toggle="modal" data-target="#clients-form" class="btn-sm btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                </th>
                                <th>{{ __('table_headers.name') }}</th>
                                <th>{{ __('table_headers.updated_at') }}</th>
                                <th>{{ __('table_headers.created_at') }}</th>
                                <th>
                                    <div class="btn-sm btn btn-success" id="reload-table">
                                        <i class="fa fa-sync"></i>
                                    </div>
                                </th>
                            </thead>
                            <tbody>
                                @php
                                    $count_table_row = 1;
                                @endphp

                                @foreach($records as $record)
                                <tr>
                                    <td>{{ ($data['page'] - 1) * $data['rows_x_page'] + ($count_table_row++) }}</td>
                                    <td>{{ $record->name }}</td>
                                    <td>{{ \Carbon\Carbon::parse( $record->updated_at )->format('d/m/Y H:i:s') }}</td>
                                    <td>{{ \Carbon\Carbon::parse( $record->created_at )->format('d/m/Y H:i:s') }}</td>
                                    <td row-data-id='{{ $record->id }}' style='min-width: 110px;' data-json = '{{ json_encode($record) }}'>
                                        <div class="clients-edit btn-sm btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </div>
                                        <div class="clients-remove btn-sm btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="clients-form" tabindex="-1" role="dialog" aria-labelledby="clients-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="clients-form-label" add-name="{{__('modals.add_client')}}" edit-name="{{__('modals.edit_client')}}">{{__('modals.add_client')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="id" value="">
            <div class="form-group">
                <label for="client-name">{{ __('modals.client_name')}}</label>
                <input type="text" name="name" class="form-control" id="client-name" placeholder="{{__('modals.client_name')}}">
                <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modals.close_default') }}</button>
            <button type="button" class="btn btn-primary">{{__('modals.save_default')}}</button>
        </div>
        </div>
    </div>
</div>
@endsection