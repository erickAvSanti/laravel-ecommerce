@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header custom-design1">
                    {{__('titles.options')}}
                </div>

                <div id="card-options" class="card-body custom-design1">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="full-scroll-x">
                        <table class="table table-hover table-borderer">
                            <thead>
                                <th>
                                    <div id="options-add" data-toggle="modal" data-target="#options-form" class="btn-sm btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                </th>
                                <th>{{ __('modals.option_key') }}</th>
                                <th>{{ __('modals.option_value') }}</th>
                                <th>{{ __('modals.option_desc') }}</th>
                                <th>{{ __('table_headers.updated_at') }}</th>
                                <!-- 
                                <th>{{ __('table_headers.created_at') }}</th>
                                -->
                                <th>
                                    <div class="btn-sm btn btn-success reload-table">
                                        <i class="fa fa-sync"></i>
                                    </div>
                                </th>
                            </thead>
                            <tbody>
                                @php
                                    $count_table_row = 1;
                                @endphp

                                @foreach($records as $record)
                                <tr>
                                    <td>{{ $count_table_row++ }}</td>
                                    <td>{{ $record->option_key }}</td>
                                    <td>{{ $record->option_value }}</td>
                                    <td>{{ $record->option_desc }}</td>
                                    <td>{{ \Carbon\Carbon::parse( $record->updated_at )->format('d/m/Y H:i:s') }}</td>
                                    <!--
                                    <td>{{ \Carbon\Carbon::parse( $record->created_at )->format('d/m/Y H:i:s') }}</td>
                                    -->
                                    <td row-data-id='{{ $record->id }}' style='min-width: 110px;' data-json = '{{ json_encode($record) }}'>
                                        
                                        <div class="options-edit btn-sm btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </div>
                                        <div class="options-remove btn-sm btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="options-form" tabindex="-1" role="dialog" aria-labelledby="options-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="options-form-label" add-name="{{__('modals.add_option')}}" edit-name="{{__('modals.edit_option')}}">{{__('modals.add_option')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <div class="form-group">
                    <label for="option-key">{{ __('modals.option_key')}}</label>
                    <input type="text" name="option_key" class="form-control" id="option-key" placeholder="{{__('modals.option_key')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label for="option-value">{{ __('modals.option_value')}}</label>
                    <input type="text" name="option_value" class="form-control" id="option-value" placeholder="{{__('modals.option_value')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
                <div class="form-group">
                    <label> {{ __('modals.option_desc') }} </label>
                    <textarea class="form-control" rows="3" name="option_desc"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modals.close_default') }}</button>
                <button type="button" class="btn btn-primary">{{__('modals.save_default')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection