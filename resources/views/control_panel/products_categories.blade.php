@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header custom-design1">
                    {{__('titles.products_categories')}}
                </div>

                <div id="card-product-categories" class="card-body custom-design1">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="full-scroll-x">
                        <table class="table table-hover table-borderer">
                            <thead>
                                <th>
                                    <div id="products-categories-add" data-toggle="modal" data-target="#products-categories-form" class="btn-sm btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                </th>
                                <th>{{ __('table_headers.name') }}</th>
                                <th>{{ __('table_headers.updated_at') }}</th>
                                <!-- 
                                <th>{{ __('table_headers.created_at') }}</th>
                                -->
                                <th>
                                    <div class="btn-sm btn btn-success reload-table">
                                        <i class="fa fa-sync"></i>
                                    </div>
                                </th>
                            </thead>
                            <tbody>
                                @php
                                    $count_table_row = 1;
                                @endphp

                                @foreach($records as $category)
                                <tr>
                                    <td>{{ $count_table_row++ }}</td>
                                    <td>{{ ( $category->parent_parent_name ? $category->parent_parent_name.' / ' : '' ). ' ' .( $category->parent_name ? $category->parent_name.' / ' : '' ). ' ' .$category->name }}</td>
                                    <td>{{ \Carbon\Carbon::parse( $category->updated_at )->format('d/m/Y H:i:s') }}</td>
                                    <!--
                                    <td>{{ \Carbon\Carbon::parse( $category->created_at )->format('d/m/Y H:i:s') }}</td>
                                    -->
                                    <td row-data-id='{{ $category->id }}' style='min-width: 110px;' data-json = '{{ json_encode($category) }}'>
                                        @if(!$category->parent_parent_id)
                                        <div class="products-subcategories-add btn-sm btn btn-success">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        @endif
                                        <div class="products-categories-edit btn-sm btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </div>
                                        <div class="products-categories-remove btn-sm btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="products-categories-form" tabindex="-1" role="dialog" aria-labelledby="products-categories-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="products-categories-form-label" add-name="{{__('modals.add_product_category')}}" edit-name="{{__('modals.edit_product_category')}}">{{__('modals.add_product_category')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <div class="form-group">
                    <label for="product-category-name">{{ __('modals.product_category_name')}}</label>
                    <input type="text" name="name" class="form-control" id="product-category-name" placeholder="{{__('modals.product_category_name')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modals.close_default') }}</button>
                <button type="button" class="btn btn-primary">{{__('modals.save_default')}}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="products-subcategories-form" tabindex="-1" role="dialog" aria-labelledby="products-subcategories-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="products-subcategories-form-label" add-name="{{__('modals.add_product_subcategory')}}" edit-name="{{__('modals.edit_product_subcategory')}}">{{__('modals.add_product_subcategory')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="parent_id" value="">
                <div class="form-group">
                    <label for="product-subcategory-name">{{ __('modals.product_subcategory_name')}}</label>
                    <input type="text" name="name" class="form-control" id="product-subcategory-name" placeholder="{{__('modals.product_subcategory_name')}}">
                    <span class='msg_empty hide'>{{ __('fields.empty') }}</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modals.close_default') }}</button>
                <button type="button" class="btn btn-primary">{{__('modals.save_default')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection