@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @php
                        $urls = include resource_path('values'.DIRECTORY_SEPARATOR.'home_urls.php');
                    @endphp
                    <ul>
                        @foreach($urls as $url)
                        <li>
                            <a href="{{ route($url['route_name']) }}">{{ $url['name'] }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
