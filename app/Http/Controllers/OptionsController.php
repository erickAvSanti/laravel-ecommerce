<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;
use \App\Option;

class OptionsController extends Controller
{
    //
    public function __construct()
    {
    }
    public function index(Request $request)
    {
		$records = &Option::get();
        return view( 'control_panel.options' ,[ 
        	'records' => $records, 
        ] );
    }
    public function create(Request $request)
    {
    	/*this prevent the use of middleware on route file, because this is a webservice*/
    	if( Auth::check() )
    	{
            try {
                return response()->json(Option::add($request));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
    	}else{
    		return response()->json([],403);
    	}
    }
    public function update(Request $request,$id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(Option::edit($request,$id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function remove($id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(Option::remove($id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
}
