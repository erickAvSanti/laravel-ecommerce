<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;
use \App\CronTab;

class CronTabController extends Controller
{
    //
    public function __construct()
    {
    }
    public function index(Request $request)
    {
		$records = &CronTab::get();
        return view( 'control_panel.crontab' ,[ 
        	'records' => $records, 
        ] );
    }
    public function create(Request $request)
    {
    	/*this prevent the use of middleware on route file, because this is a webservice*/
    	if( Auth::check() )
    	{
            try {
                return response()->json(CronTab::add($request));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
    	}else{
    		return response()->json([],403);
    	}
    }
    public function update(Request $request,$id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(CronTab::edit($request,$id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function remove($id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(CronTab::remove($id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function save_to_file()
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(CronTab::save_to_file());
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
}
