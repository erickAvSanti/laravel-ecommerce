<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\ProductCategory;
use \Auth;
use \App\Http\Requests\ProductCategoryRequest;

class ProductsCategoriesController extends Controller
{
    //
    public function index(Request $request)
    {
		$records = &ProductCategory::get( $request , $request->page );
        return view( 'control_panel.products_categories' ,[ 
        	'records' => $records, 
        ] );
    }
    public function create(ProductCategoryRequest $request)
    {
    	/*this prevent the use of middleware on route file, because this is a webservice*/
    	if( Auth::check() )
    	{
            try {
                return response()->json(ProductCategory::add($request));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
    	}else{
    		return response()->json([],403);
    	}
    }
    public function update(ProductCategoryRequest $request,$id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(ProductCategory::edit($request,$id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function remove($id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(ProductCategory::remove($id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
}
