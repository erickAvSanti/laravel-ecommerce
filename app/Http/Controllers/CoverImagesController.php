<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \App\CoverImage;
use \Auth;

class CoverImagesController extends Controller
{
    //
    const RELATIVE_PATH = 'public/cover_images';
    const RELATIVE_PATH_STORAGE = 'storage/cover_images';

    public function index(Request $request)
    {
        return view( 'control_panel.cover_images' ,[ 
        	'records' => CoverImage::all(),
        	'relative_path' => self::RELATIVE_PATH_STORAGE,
        ]);
    }
    public function create(Request $request)
    {
    	$files = $request->file('file');
    	if(!is_array($files)) $files = array( $file );
    	$records = array();
    	foreach ($files as $file) {
    		$path = $file->store(self::RELATIVE_PATH);
	    	if($path)
	    	{
	    		$record = new CoverImage();
	    		$record->name = $file->getClientOriginalName();
	    		$record->filename = str_replace( self::RELATIVE_PATH, '' , $path );
	    		try {

	    			$record->save();
	    			$records[] = $record;

	    		} catch ( \Exception $e ) {

	    			Storage::delete( $path );
	    			return response()->json( [ 'msg'=>$e->getMessage() ] , 422 );

	    		}
	    	}else
	    	{
	    		return response()->json( [ 'msg'=>'file not stored '.$file->getClientOriginalName() ] , 422 );
	    	}
	    	
    	}
    	return response()->json($records);
    }
    public function list(Request $request = null)
    {
		return response()->json(CoverImage::all());
    }

    public function remove($id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(CoverImage::remove($id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function update(Request $request,$id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(CoverImage::edit($request,$id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
}
