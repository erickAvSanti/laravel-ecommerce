<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Product;
use \App\ProductCategory;
use \Auth;
use \App\Http\Requests\ProductRequest;

class ProductsController extends Controller
{
    //
    public function __construct()
    {
    }
    public function index(Request $request)
    {
    	$search = $request->search ? $request->search : '';
		$data = &Product::get( $request , $request->page );
   		$records = array_key_exists( "records" , $data ) ? $data[ 'records' ] : [];
        return view( 'control_panel.products' ,[ 
        	'data' => $data , 
        	'records' => $records, 
        	'search' => $search,
        	'pagination' => Controller::pag($data[ 'page' ], $data[ 'total_pages' ]),
            'categories' => ( $categories = &ProductCategory::get(false) )
        ] );
    }
    public function create(ProductRequest $request)
    {
    	/*this prevent the use of middleware on route file, because this is a webservice*/
    	if( Auth::check() )
    	{
            try {
                return response()->json(Product::add($request));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
    	}else{
    		return response()->json([],403);
    	}
    }
    public function update(ProductRequest $request,$id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(Product::edit($request,$id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function remove($id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(Product::remove($id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
}
