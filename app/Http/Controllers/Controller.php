<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    

    public static function push2( &$_arr , $val )
    {
        $found = false;
        for($i = 0; $i < count($_arr) ; $i++)
        {
            if($_arr[$i] == $val && $val != '...'){
                $found = true;
                break;
            }
        }
        if(!$found)
        {
            $_arr[] = $val;
        }
        return $found;
    }
    public static function pag($current,$total)
    {
        $arr = array();
        if( $total > 7 )
        {
            if( $current < 5 )
            {
                for( $i=1 ; $i < 5 ; $i++ ){
                    self::push2( $arr , $i );
                }
                self::push2( $arr , 5 );
                self::push2( $arr , '...' );
                self::push2( $arr , $total-2 );
                self::push2( $arr , $total-1 );
                self::push2( $arr , $total );
            }
            else if( $current > $total-4 )
            {
                self::push2( $arr , 1 );
                self::push2( $arr , 2 );
                self::push2( $arr , 3 );
                self::push2( $arr , 4 );
                self::push2( $arr , '...' );
                for( $i = $total-4 ; $i <= $total ; $i++ ){
                    self::push2( $arr , $i );
                }
            }
            else
            {
                self::push2( $arr , 1 );
                self::push2( $arr , 2 );
                self::push2( $arr , 3 );
                self::push2( $arr , 4 );
                self::push2( $arr , '...' );
                self::push2( $arr , $current-1 );
                self::push2( $arr , $current );
                self::push2( $arr , $current+1 );
                self::push2( $arr , '...' );
                self::push2( $arr , $total-2 );
                self::push2( $arr , $total-1 );
                self::push2( $arr , $total );
            }
        }
        else
        {
            for( $i=1 ; $i <= $total ; $i++ ){
                self::push2( $arr , $i );
            }
        }
        $removeIndex = array();
        for( $i=0 ; $i < count($arr)-2 ; $i++){
            $v1 = $arr[ $i ];
            $v2 = $arr[ $i + 1 ];
            $v3 = $arr[ $i + 2 ];
            if(is_numeric( $v1 ) && is_numeric( $v3 ) && $v2 == '...' ){
                if( $v3 - $v1 == 1 )
                {
                    $removeIndex[] = $i + 1;
                }
              
            }
        }
        for( $i = count( $arr )-1 ; $i >= 2 ; $i-- ){
            $v1 = $arr[ $i - 2 ];
            $v2 = $arr[ $i - 1 ];
            $v3 = $arr[ $i ];
            if(is_numeric( $v1 ) && is_numeric( $v3 ) && $v2 == '...' ){
                if( $v3 - $v1 == 1 )
                {
                    $removeIndex[] = $i - 1;
                }
            }
        }
        for( $i = 0 ; $i < count( $arr ); $i++ ){
            $midd = $arr[ $i ];
            $v_prev = array_key_exists($i - 1, $arr ) ? $arr[ $i - 1 ] : null;
            $v_next = array_key_exists($i + 1, $arr ) ? $arr[ $i + 1 ] : null;
            if( is_numeric( $v_prev ) && is_numeric( $v_next ) && $midd == '...' ){
                if( $v_next - $v_prev == 2 ){
                    $arr[$i] = $v_prev + 1;
                }
              
            }
        }
        if( count( $removeIndex ) > 0 ){
            $arr2 = array();
            for( $i = 0 ; $i < count( $arr ) ; $i++ ){
                $ff = false;
                for( $j = 0 ; $j < count( $removeIndex ); $j++ ){
                    if( $i == $removeIndex[$j] )
                    {
                        $ff = true;
                        break;
                    }
                }
                if(!$ff){
                    $arr2[] = $arr[$i];
                }
            }
            $arr = $arr2;
        }
        return $arr;
    }
}
