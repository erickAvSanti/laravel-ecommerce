<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Client;
use \Auth;
use \App\Http\Requests\ClientRequest;

class ClientsController extends Controller
{
    //
    public function __construct()
    {
    }
    public function index(Request $request)
    {
    	$search = $request->search ? $request->search : '';
		$data = &Client::get( $request , $request->page );
   		$records = array_key_exists( "records" , $data ) ? $data[ 'records' ] : [];
        return view( 'control_panel.clients' ,[ 
        	'data' => $data , 
        	'records' => $records, 
        	'search' => $search,
        	'pagination' => Controller::pag($data[ 'page' ], $data[ 'total_pages' ]),
        ] );
    }
    public function create(ClientRequest $request)
    {
    	/*this prevent the use of middleware on route file, because this is a webservice*/
    	if( Auth::check() )
    	{
            try {
                return response()->json(Client::add($request));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
    	}else{
    		return response()->json([],403);
    	}
    }
    public function update(ClientRequest $request,$id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(Client::edit($request,$id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
    public function remove($id)
    {
        /*this prevent the use of middleware on route file, because this is a webservice*/
        if( Auth::check() )
        {
            try {
                return response()->json(Client::remove($id));
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()],406);
            }
        }else{
            return response()->json([],403);
        }
    }
}
