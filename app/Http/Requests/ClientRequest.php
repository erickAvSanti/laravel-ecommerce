<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rule = [];
        switch($this->method())
        {
            case 'POST':
            case 'post':
                $rule = [
                    'name' => 'required|string|unique:products|between:4,100',
                ];
            break;
            case 'PUT':
            case 'put':
                $rule = [
                    'name' => 'required|string|between:4,200|unique:products,name,'.$request->id,
                ];
            break;
        }
        return $rule;
    }
    public function messages()
    {
        return [
            'name.required' => 'This field is required',
            'name.between' => 'This field must have a length between 4 and 100',
            'name.unique' => 'This field must be unique',
        ];
    }
}
