<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CoverImage extends Model
{
    //
    public static function remove($id)
    {
        $record = CoverImage::find($id);
        if( !$record )
        {
        	throw new \Exception('Record not found with id '.$id);
        }
        $record->delete();
        Storage::delete( \App\Http\Controllers\CoverImagesController::RELATIVE_PATH . $record->filename );
        return $record;
    }

    public static function edit($request,$id)
    {
        $record = CoverImage::find($id);
        if( !$record )
        {
        	throw new \Exception('Record not found with id '.$id);
        }
        
        if( $request->title ) $record->title = $request->title;
        if( $request->subtitle1 ) $record->subtitle1 = $request->subtitle1;
        if( $request->subtitle2 ) $record->subtitle2 = $request->subtitle2;

        $record->save();

        return $record;
    }
}
