<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronTab extends Model
{
    //
    protected $fillable = [
    	'c_minute',
    	'c_hour',
    	'c_day_of_month',
    	'c_month',
    	'c_day_of_week',
    	'c_command',
    ];

    public static function &get()
    {
    	self::updateExistingCronJobs();
        $records = CronTab::all();
        return $records;
    }

    private static function cleanSpaces($str){
    	if(!is_string($str))return '';
    	return trim(preg_replace("/[\n\r\t ]+/"," ",$str));
    }

    public static function add($request)
    {
        $record = new CronTab();
        if( $request->c_minute ) 			$record->c_minute 		= $request->c_minute;
        if( $request->c_hour ) 				$record->c_hour 		= $request->c_hour;
        if( $request->c_day_of_month ) 		$record->c_day_of_month = $request->c_day_of_month;
        if( $request->c_month ) 			$record->c_month 		= $request->c_month;
        if( $request->c_day_of_week ) 		$record->c_day_of_week 	= $request->c_day_of_week;
        if( $request->c_command ) 			$record->c_command 		= self::cleanSpaces($request->c_command);
        $record->save();
        self::save_to_file();
        return $record;
    }
    public static function edit($request,$id)
    {
        $record = CronTab::find($id);
        if( $request->c_minute ) 			$record->c_minute 		= $request->c_minute;
        if( $request->c_hour ) 				$record->c_hour 		= $request->c_hour;
        if( $request->c_day_of_month ) 		$record->c_day_of_month = $request->c_day_of_month;
        if( $request->c_month ) 			$record->c_month 		= $request->c_month;
        if( $request->c_day_of_week ) 		$record->c_day_of_week 	= $request->c_day_of_week;
        if( $request->c_command ) 			$record->c_command 		= self::cleanSpaces($request->c_command);
        $record->save();
        self::save_to_file();
        return $record;
    }
    public static function remove($id)
    {
        $record = CronTab::find($id);
        $record->delete();
        return $record;
    }
    private static function &getCronJobs(){

        $res = shell_exec("crontab -l");
        $res = explode("\n",$res);
        $valid = array();
        foreach ($res as $key => $value) {
        	$value = trim($value);
        	if(substr($value,0,1)=="#" || empty($value))continue;
        	$value = explode(" ",$value);
        	$tmp = [$value[0],$value[1],$value[2],$value[3],$value[4]];
        	unset($value[0],$value[1],$value[2],$value[3],$value[4]);
        	$tmp[] = implode(" ",$value);
        	$valid[] = $tmp;
        }
        return $valid;
    }
    private static function updateExistingCronJobs(){
    	$values = &self::getCronJobs();
    	CronTab::query()->delete();
    	foreach ($values as $key => $value) {
    		CronTab::create([
    			'c_minute'=> $value[0],
    			'c_hour'=> $value[1],
    			'c_day_of_month'=> $value[2],
    			'c_month'=> $value[3],
    			'c_day_of_week'=> $value[4],
    			'c_command'=> $value[5],
    		]);
    	}
    }
    public static function save_to_file()
    {
    	$crontab_backup = base_path()."/crontab.".time().".backup";
    	\Log::info($crontab_backup);
    	shell_exec("crontab -l > ".$crontab_backup);
        $records = CronTab::all();
        $str = "";
        foreach ($records as &$record) {
        	$str .= $record->c_minute." ".$record->c_hour." ".$record->c_day_of_month." ".$record->c_month." ".$record->c_day_of_week." ".$record->c_command."\n";
        } 
        $file = tmpfile();
        fwrite($file,$str);
		$path = stream_get_meta_data($file)['uri'];
        $res = shell_exec("cat $path | crontab -");
		fclose($file);
        return $str;
    }
}
