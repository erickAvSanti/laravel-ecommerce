<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;
class ProductCategory extends Model
{
    //
	public function getParentParentIdAttribute($value)
    {
        return $value;
    }

	public function setParentParentIdAttribute($value)
    {
        $this->attributes['parent_parent_id'] = $value;
    }

	public function getParentNameAttribute($value)
    {
        return $value;
    }

	public function setParentNameAttribute($value)
    {
        $this->attributes['parent_name'] = $value;
    }

	public function getParentParentNameAttribute($value)
    {
        return $value;
    }

	public function setParentParentNameAttribute($value)
    {
        $this->attributes['parent_parent_name'] = $value;
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'product_category','product_id','category_id');
    }

    public function children()
    {
        return $this->hasMany('App\ProductCategory','parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\ProductCategory','parent_id');
    }

    protected static function boot()
	{
	    parent::boot();

	    static::deleting(function($record) {
	        $relationMethods = ['children'];

	        foreach ($relationMethods as $relationMethod) {
	            if ($record->$relationMethod()->count() > 0) {
	                throw new \Exception('This record has dependencies');
	            }
	        }
	    });
	}

    public static function &get($all = true)
    {
        $records = ProductCategory::whereNull('parent_id')->orderBy('name','ASC')->get();
        $arr = array();
        foreach ($records as &$record) {
        	$arr[] = $record;
        	if( $all )
        	{
        		$sub1 = ProductCategory::where('parent_id',$record->id)->orderBy('name','ASC')->get();
	        	foreach ($sub1 as $sub_record1) {
	        		$sub_record1->parent_name = $record->name;
	        		$arr[] = $sub_record1;
		        	$sub2 = ProductCategory::where('parent_id',$sub_record1->id)->orderBy('name','ASC')->get();
		        	foreach ($sub2 as $sub_record2) {
	        			$sub_record2->parent_name = $sub_record1->name;
	        			$sub_record2->parent_parent_name = $record->name;
	        			$sub_record2->parent_parent_id = $record->id;
	        			$arr[] = $sub_record2;
		        	}
	        	}
        	}
        }
    	return $arr;
    }

    public static function add($request)
    {
    	if($request->parent_id)
    	{
        	$parent = ProductCategory::find($request->parent_id);
        	if(!$parent)
        	{
       	 		throw new \Exception('Parent record not found');
        	}
	        $record = new ProductCategory();
	        $record->parent_id = $request->parent_id;
	        $record->name = $request->name;
	        $record->save();
    	}else
    	{
	        $record = new ProductCategory();
	        $record->name = $request->name;
	        $record->save();
    	}
        return $record;
    }
    public static function edit($request,$id)
    {
        $record = ProductCategory::find($id);
        $record->name = $request->name;
        $record->save();
        return $record;
    }
    public static function remove($id)
    {
        $record = ProductCategory::find($id);
        $record->delete();
        return $record;
    }
}
