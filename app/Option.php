<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //

    public static function &get()
    {
        $records = Option::all();
        return $records;
    }

    public static function add($request)
    {
        $record = new Option();
        if( $request->option_key ) $record->option_key = $request->option_key;
        if( $request->option_value ) $record->option_value = $request->option_value;
        if( $request->option_desc ) $record->option_desc = $request->option_desc;
        $record->save();
        return $record;
    }
    public static function edit($request,$id)
    {
        $record = Option::find($id);
        if( $request->option_key ) $record->option_key = $request->option_key;
        if( $request->option_value ) $record->option_value = $request->option_value;
        if( $request->option_desc ) $record->option_desc = $request->option_desc;
        $record->save();
        return $record;
    }
    public static function remove($id)
    {
        $record = Option::find($id);
        $record->delete();
        return $record;
    }
}
