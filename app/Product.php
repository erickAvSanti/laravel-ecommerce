<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	const ROWS_X_PAGE = 20;
    //

    public function getCategoryIdsAttribute($value)
    {
        return $value;
    }

    public function setCategoryIdsAttribute($value)
    {
        $this->attributes['category_ids'] = $value;
    }

    public static function &get(&$request = null,$page = 1)
    {
        if( $page < 1 ) $page = 1;
    	$records = Product::select( "*" );
        if($request->search && strlen($request->search)>0){
            $records = 
                $records->where('name','like',"%{$request->search}%")
                ->orWhere('sku', 'like',"%{$request->search}%");
        }
    	$total = $records->count();
    	$total_pages = ( int ) ( $total / self::ROWS_X_PAGE ) + ( ( $total % self::ROWS_X_PAGE ) > 0 ? 1 : 0 );
    	if($page > $total_pages)
        {
    		$page = $total_pages;
    	}
    	$records = $records->offset( ( $page - 1 ) * self::ROWS_X_PAGE )->limit( self::ROWS_X_PAGE )->get();
    	$data = [];

        foreach ($records as &$record) {
            $record->category_ids = $record->categories()->pluck('category_id');
        }

    	$data[ 'records' ] = $records;
    	$data[ 'page' ] = $page;
    	$data[ 'total_pages' ] = $total_pages;
    	$data[ 'rows_x_page' ] = self::ROWS_X_PAGE;

    	return $data;
    }

    public static function add($request)
    {
        $record = new Product();
        $record->name = $request->name;
        $record->sku = $request->sku;
        $record->save();
        if($request->categories)
        {
            $cats = json_decode($request->categories,true);
            if( is_array( $cats ) )
            {
                foreach ($cats as &$cat_id) {
                    $record->categories()->attach($cat_id);
                }
            }
        }
        return $record;
    }
    public static function edit($request,$id)
    {
        $record = Product::find($id);
        $record->name = $request->name;
        $record->sku = $request->sku;
        $record->save();
        if($request->categories)
        {
            $cats = json_decode($request->categories,true);
            if( is_array( $cats ) )
            {
                foreach ($cats as &$cat_id) {
                    $record->categories()->attach($cat_id);
                }
            }
        }
        return $record;
    }
    public static function remove($id)
    {
        $record = Product::find($id);
        $record->delete();
        return $record;
    }

    public function categories()
    {
        return $this->belongsToMany('App\ProductCategory','product_category','product_id','category_id');
    }
}
