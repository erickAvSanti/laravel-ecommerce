<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	const ROWS_X_PAGE = 20;
    //

    public static function &get(&$request = null,$page = 1)
    {
        if( $page < 1 ) $page = 1;
    	$records = Client::select( "*" );
        if($request->search && strlen($request->search)>0){
            $records = 
                $records->where('name','like',"%{$request->search}%")
                ->orWhere('document', 'like',"%{$request->search}%")
                ->orWhere('phone_number1', 'like',"%{$request->search}%")
                ->orWhere('phone_number2', 'like',"%{$request->search}%");
        }
    	$total = $records->count();
    	$total_pages = ( int ) ( $total / self::ROWS_X_PAGE ) + ( ( $total % self::ROWS_X_PAGE ) > 0 ? 1 : 0 );
    	if($page > $total_pages)
        {
    		$page = $total_pages;
    	}
    	$records = $records->offset( ( $page - 1 ) * self::ROWS_X_PAGE )->limit( self::ROWS_X_PAGE )->get();
    	$data = [];

    	$data[ 'records' ] = $records;
    	$data[ 'page' ] = $page;
    	$data[ 'total_pages' ] = $total_pages;
    	$data[ 'rows_x_page' ] = self::ROWS_X_PAGE;

    	return $data;
    }

    public static function add($request)
    {
        $record = new Client();
        $record->name = $request->name;
        $record->save();
        return $record;
    }
    public static function edit($request,$id)
    {
        $record = Client::find($id);
        $record->name = $request->name;
        $record->save();
        return $record;
    }
    public static function remove($id)
    {
        $record = Client::find($id);
        $record->delete();
        return $record;
    }
}
