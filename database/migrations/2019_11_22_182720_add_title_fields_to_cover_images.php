<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTitleFieldsToCoverImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cover_images', function (Blueprint $table) {
            $table->string( 'title' , 120 )->default( '' );
            $table->string( 'subtitle1' , 120 )->default( '' );
            $table->string( 'subtitle2' , 120 )->default( '' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cover_images', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('subtitle1');
            $table->dropColumn('subtitle2');
        });
    }
}
