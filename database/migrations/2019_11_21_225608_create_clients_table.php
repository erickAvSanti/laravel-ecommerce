<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100);
            $table->date('onomastic')->nullable();
            $table->string('document',50)->default('');
            $table->string('address',400)->default('');
            $table->string('phone_number1',50)->default('');
            $table->string('phone_number2',50)->default('');
            $table->string('email1',50)->default('');
            $table->string('email2',50)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
