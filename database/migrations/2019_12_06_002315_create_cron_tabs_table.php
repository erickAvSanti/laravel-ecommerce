<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCronTabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_tabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('c_minute',10)->default('*');
            $table->string('c_hour',10)->default('*');
            $table->string('c_day_of_month',10)->default('*');
            $table->string('c_month',10)->default('*');
            $table->string('c_day_of_week',10)->default('*');
            $table->string('c_command',300)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_tabs');
    }
}
