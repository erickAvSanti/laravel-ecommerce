<?php

use Illuminate\Database\Seeder;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $records = [
        	[
	            'option_key' => 'fb_url',
                'option_value' => 'https://fb.com',
                'option_desc' => '',
	        ],
        	[
	            'option_key' => 'youtube_url',
                'option_value' => 'https://youtube.com',
                'option_desc' => '',
	        ],
        	[
	            'option_key' => 'twitter_url',
                'option_value' => 'https://twitter.com',
                'option_desc' => '',
	        ],
        	[
	            'option_key' => 'instagram_url',
                'option_value' => 'https://instagram.com',
                'option_desc' => '',
	        ],
        ];
        foreach ($records as &$record) {
        	$found = \App\Option::where( 'option_key' , $record[ 'option_key' ] );
        	if( $found->count()==0 )
        	{
        		echo " creating : ".print_r($record,true);
        		App\Option::create($record);
        	}else{
        		echo " record already exists ".print_r($record,true);
        	}
        }
    }
}
