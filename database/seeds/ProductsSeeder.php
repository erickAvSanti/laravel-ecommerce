<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
        	
        	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
	    	[
	            'name' => Str::random(10),
	            'sku' => Str::random(20),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
	        ],
        ]
    	);
    }
}
